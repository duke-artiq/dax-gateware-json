{ pkgs ? import <nixpkgs> {} }:

let
  dax-full = import <dax-full> { inherit pkgs; };
in
  pkgs.mkShell {
    buildInputs = [
      (pkgs.python3.withPackages(ps: [
        dax-full.dax-gateware
      ]))
    ];
  }
