# DAX gateware JSON

JSON descriptions of Duke Kasli devices - public for CI/CD reasons.

Note that the names of gateware files and the corresponding variant name are important depending on the chosen base.
The names are used by the `artiq_flash` command to upload the correct runtime.

- `master` gateware should be named as `*_master`
- `satellite` gateware should be named as `*_satellite`
- `standalone` gateware can have any name that does not conflict with the other base descriptions

# Nix Scripts
Boards need to be added to the [nix scripts](https://gitlab.com/duke-artiq/nix-scripts) repository to build. 

For Artiq 6, see: [dax-full.nix](https://gitlab.com/duke-artiq/nix-scripts/-/blob/release-6/dax-full.nix?ref_type=heads) in the `release-6` branch

For Artiq 7, see: [dax-board/flake.nix](https://gitlab.com/duke-artiq/nix-scripts/-/blob/master/dax-board/flake.nix?ref_type=heads) in the `release-7` branch

# Gateware json Definitions
For the gateware json definitions, see: [artiq/coredevice/coredevice_generic.schema.json](https://github.com/m-labs/artiq/blob/da15e94c22c2979771156695da19314b63f78e85/artiq/coredevice/coredevice_generic.schema.json)